<?php

namespace Drupal\geodeny;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\geodeny\Form\GeoDenyConfigForm;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\ip2country\Ip2CountryLookup;

class GeoDenyResponseSubscriber implements EventSubscriberInterface
{
  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * IP2Country Lookup.
   *
   * @var \Drupal\ip2country\Ip2CountryLookup
   */
  protected $ip2CountryLookUp;

  /**
   * The Bito configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  public function __construct(RequestStack $requestStack, Ip2CountryLookup $ip2CountryLookup, ConfigFactoryInterface $configFactory)
  {
    $this->currentRequest = $requestStack->getCurrentRequest();
    $this->ip2CountryLookUp = $ip2CountryLookup;
    $this->config = $configFactory->get(GeoDenyConfigForm::CONFIG_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents()
  {
    $events[KernelEvents::RESPONSE][] = 'alterResponse';
    return $events;
  }


  public function alterResponse(ResponseEvent $event)
  {
    $ip = $this->currentRequest->getClientIp();
    if ($country = $this->ip2CountryLookUp->getCountry($ip)) {
      if (in_array($country, $this->config->get('geoList') ?? [])) {
        $event->setResponse(new Response('', 400));
      }
    }
  }
}
