<?php

namespace Drupal\geodeny\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Locale\CountryManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class GeoDenyConfigForm.
 */
class GeoDenyConfigForm extends ConfigFormBase {

  /**
   * This config name.
   */
  const CONFIG_NAME = 'geodeny.config';

  /**
   * The country manager.
   *
   * @var \Drupal\Core\Locale\CountryManagerInterface
   */
  protected $countryManager;

  /**
   * Class constructor.
   */
  public function __construct(CountryManagerInterface $countryManager) {
    $this->countryManager = $countryManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('country_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'geodeny_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_NAME);

    $form['geoList'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Country/Region'),
      '#options' => $this->countryManager->getList(),
      '#default_value' => $config->get('geoList') ?? [],
      '#multiple' => TRUE,
    ];

    // @todo: Implement negation.

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config(self::CONFIG_NAME)
      ->set('geoList', $form_state->getValue('geoList'))->save();
  }

}
